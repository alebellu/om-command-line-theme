; this project has been created with ...
; to build dev version: lein cljsbuild auto dev
; to build release version: lein cljsbuild auto release
; to build the css: lein garden auto
; to clean all: lein clean
(ns ^:figwheel-always reagent-command-line-theme.core
  (:require [cljs.core.async :as async :refer [<! chan close! put!]]
            [cljs.core.async.impl.protocols :refer [Channel]]
            [cljs-time.core :as t]
            [cljs-time.coerce :as c]
            [cljs-time.format :as f]
            [taoensso.timbre :as timbre :refer-macros (log trace debug info warn error fatal)]
            [goog.dom :as gdom]
            [om.next :as om :refer-macros [defui]]
            [om.dom :as dom]
            [sablono.core :as html :refer-macros [html]]
            [ssoup-clj-core.core :as s]
            [ssoup-clj-client.core :as sc]
            [ssoup-clj-client.session :as session]
            [ssoup-clj-client.auth :as auth]
            [ssoup-clj-client.i18n :refer [msg]]
            [ssoup-om-client.core :refer [show container]]
            [om-command-line-theme.scs-rules]
            [om-command-line-theme.module])
  (:require-macros [cljs.core.async.macros :refer [go]]))

(def jquery (js* "$"))

(defn empty-viewer [ctx data]
  [:div.nil])

(defn datatable-render [ctx data]
  [:table#example.table.table-striped.table-bordered {:cell-spacing "0" :width "100%"}
   [:thead
    [:tr
     (for [key (:keys data)]
       ^{:key key} [:th (name key)])]]
   [:tbody
    (for [item (:items data)]
      ^{:key (:uuid item)} [:tr
                            (for [key (:keys data)]
                              ^{:key key} [:td [show ctx (get item (keyword key))]])])]])

(defn datatable-did-mount [ctx data]
  (.ready (js/$ js/document) (fn []
                               (.DataTable (js/$ "#example")))))

(defn datatable-viewer [ctx data]
  (info "call dtb " data)
  (reagent/create-class {:reagent-render      (fn [] (datatable-render ctx data))
                         :component-did-mount (fn [] (datatable-did-mount ctx data))}))

(defn dashboard-2-2-1 [ctx data]
  "A vector viewer displaying items in a 2-2-1 dashboard layout"
  (let [size (s/array-count data)]
    [:div.col-xs-12
     [:div.col-xs-12
      [:div.col-sm-6 (when (> size 0) [show ctx (s/array-nth data 0)])]
      [:div.col-sm-6 (when (> size 1) [show ctx (s/array-nth data 1)])]]
     [:div.col-xs-12
      [:div.col-sm-6 (when (> size 2) [show ctx (s/array-nth data 2)])]
      [:div.col-sm-6 (when (> size 3) [show ctx (s/array-nth data 3)])]]
     [:div.col-xs-12
      [:div.col-sm-12 (when (> size 4) [show ctx (s/array-nth data 4)])]]]))

(defn dashboard-1-2 [ctx data]
  "A vector viewer displaying items in a 1-2 dashboard layout"
  (let [size (s/array-count data)]
    [:div.col-xs-12
     [:div.col-xs-12
      [:div.col-sm-12 (when (> size 0) [show ctx (s/array-nth data 0)])]]
     [:div.col-xs-12
      [:div.col-sm-6 (when (> size 1) [show ctx (s/array-nth data 1)])]
      [:div.col-sm-6 (when (> size 2) [show ctx (s/array-nth data 2)])]]]))

(defn dashboard-viewer [{options :options :as ctx} data]
  "A vector viewer displaying items in a dashboard layout"
  (let [layout (:layout options)]
    (cond (= "2-2-1" layout) (dashboard-2-2-1 ctx data)
          (= "1-2" layout) (dashboard-1-2 ctx data)
          :else (dashboard-2-2-1 ctx data))))

(defn flow-viewer [ctx data]
  "A vector viewer displaying items in a flow layout"
  [:div.col-xs-12
   (for [item data]
     ^{:key (:uuid item)} [show ctx item])])

(defn main-template [ctx]
  [:div.col-xs-12
   (container "console-content")])

(defn- string-viewer [_ str]
  [:span str])

(defn- string-selector [_ options]
  [:div
   [:input {:type      "text"
            :on-key-up #(let [on-key-up (:on-key-up options)]
                         (when on-key-up
                           (on-key-up (.-which %))))
            :on-change #(let [on-change (:on-change options)]
                         (when on-change
                           (on-change (-> % .-target .-value))))}]])

(defn- enum-input [enum-values on-change]
  (let [disabled (reagent/atom false)]
    (reagent/create-class
      {:reagent-render      (fn [] [:input {:type        "text"
                                            :disabled    @disabled
                                            :on-key-down #(let [ch (.-which %)]
                                                           (when-not (or (= ch 8) (and (>= ch 48) (<= ch 57))) ; backspace or digit
                                                             (.preventDefault %)))
                                            :on-key-up
                                                         #(when (= (.-which %) 13) ; carriage return
                                                           (let [selected-index (js/parseInt (-> % .-target .-value))]
                                                             (when (and (> selected-index 0) (<= selected-index (count enum-values)))
                                                               (reset! disabled true)
                                                               (on-change (nth enum-values (dec selected-index))))))}])
       :component-did-mount (fn [comp]
                              (let [el (jquery (reagent/dom-node comp))]
                                (-> (jquery "html, body")
                                    (.animate (clj->js {:scrollTop (-> el (.offset) (.-top))}) 1000))
                                (.focus el)))})))

(defn- enum-selector [ctx options]
  (let [on-change (:on-change options)
        enum-type (:obj-type ctx)
        enum-values (:enum-values enum-type)]
    [:div.col-xs-12
     [:div.col-xs-1 " "]
     [:div.col-xs-11
      [:ol
       (for [enum-value enum-values]
         ^{:key enum-value} [:li [:a {:onClick #(on-change enum-value) :title enum-value :target "_blank"} enum-value]])]]
     [:div.col-xs-12 "> Make your choice "
      [enum-input enum-values on-change]]]))

(defonce history (reagent/atom [:div]))

(comment
  (defrecord CommandLineLayoutManager
    []
    sc/ILayoutManager
    (get-layout [_ ctx]
      (:layout ctx))
    (set-layout! [_ ctx layout]
      (debug "Layout is ignored with CommandLineLayoutManager"))
    (set-content! [_ ctx content]
      (swap! history conj [:div {:key (str "command-" (count @history))} content])
      (sc/set-container-content! (s/get-engine) "console-content" @history))
    (delete-named-container! [_ container])))

(defn init-theme [ctx options]
  (debug "Registering theme command-line")

  (sc/register-theme! (s/get-engine) ctx :ssoup/command-line-theme
                      {:main-template main-template
                       :viewers       [[:ssoup-webapp/empty-viewer nil nil empty-viewer]
                                       [:ssoup-webapp/string-viewer nil :ssoup/string string-viewer]
                                       [:ssoup-webapp/flow-viewer nil :ssoup/array flow-viewer]
                                       [:ssoup-webapp/dashboard-viewer nil :ssoup/array dashboard-viewer]
                                       ;[:ssoup-webapp/datatable-viewer nil :ssoup/array datatable-viewer]
                                       ]
                       :editors       []
                       :selectors     [[:ssoup-webapp/string-selector nil :ssoup/string string-selector]
                                       [:ssoup-webapp/main-menu nil :ssoup/enum enum-selector]]}))

(derive :ssoup/command-line-theme :ssoup/default-theme)

(s/register! {:fname     :command-line-theme/init-theme
              :handler   (fn [ctx args options]
                           (init-theme ctx options))
              :flavor    :ssoup/command-line-theme
              :user-role :ssoup/any-user
              :classes   #{:ssoup/init-theme}
              :arg-types {}})
