(ns ^:figwheel-always om-command-line-theme.module
  #?(:clj  (:require [ssoup-clj-core.modules :refer [defmodule]]
                     [ssoup-clj-client.modules]
                     [om-command-line-theme.scs-rules]))
  #?(:cljs (:require [ssoup-clj-core.modules]
                     [ssoup-clj-client.modules]
                     [om-command-line-theme.scs-rules]))
  #?(:cljs (:require-macros [ssoup-clj-core.modules :refer [defmodule]])))

(defmodule om-command-line-theme "1.0.0" :ssoup-clj-client
  :css ["css/om-command-line-theme.css"]
  :scs-rules om-command-line-theme.scs-rules/rules
  :dependencies
    [["jquery"    "1.11.3"
      {:js  "https://code.jquery.com/jquery-1.11.3.min.js"}]
     ["bootstrap" "3.3.4"
      {:js  "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"
       :css ["https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"
             "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css"]}]
     ["bootstrap-datepicker" "1.4.0"
      {:js  "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.0/js/bootstrap-datepicker.min.js"
       :css "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.0/css/bootstrap-datepicker.min.css"}]
     ["datatables" "1.10.7"
      {:js  ["//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"
             "//cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js"]
       :css "//cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.css"}]])
