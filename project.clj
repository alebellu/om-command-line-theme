(defproject om-command-line-theme "0.1.0-SNAPSHOT"
  :description "FIXME: write this!"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :dependencies [[org.clojure/clojure "1.7.0"]
                 [org.clojure/clojurescript "1.7.228"]
                 [org.clojure/core.async "0.1.346.0-17112a-alpha"]
                 [com.taoensso/timbre "4.2.1"]
                 [com.andrewmcveigh/cljs-time "0.3.14"]
                 [garden "1.2.5"]
                 [org.omcljs/om "1.0.0-alpha22"]
                 [sablono "0.5.3"]
                 [com.cognitect/transit-cljs "0.8.225"]
                 [cljs-http "0.1.37"]]

  :plugins [[lein-cljsbuild "1.1.2"]
            [lein-garden "0.2.6"]
            [cljs-http "0.1.37"]]

  ; to autoreload checkouts: https://github.com/bhauman/lein-figwheel/issues/9
  :source-paths ["src" "checkouts/ssoup-om-client/src"]

  :clean-targets ^{:protect false} ["resources/public/js/compiled" "target"]
  
  :cljsbuild {
    :builds [{:id "dev"
              :source-paths ["src"]

              :compiler {:asset-path "js/compiled/out"
                         :output-to "resources/public/js/compiled/om_command_line_theme.js"
                         :output-dir "resources/public/js/compiled/out"
                         :source-map-timestamp true }}
             {:id "min"
              :source-paths ["src"]
              :compiler {:output-to "resources/public/js/compiled/om_command_line_theme.js"
                         :optimizations :advanced
                         :pretty-print false}}]}

  :figwheel {
             ;; :http-server-root "public" ;; default and assumes "resources"
             ;; :server-port 3449 ;; default
             :css-dirs ["resources/public/css"] ;; watch and update CSS

             ;; Start an nREPL server into the running figwheel process
             ;; :nrepl-port 7888

             ;; Server Ring Handler (optional)
             ;; if you want to embed a ring handler into the figwheel http-kit
             ;; server, this is for simple ring servers, if this
             ;; doesn't work for you just run your own server :)
             ;; :ring-handler hello_world.server/handler

             ;; To be able to open files in your editor from the heads up display
             ;; you will need to put a script on your path.
             ;; that script will have to take a file path and a line number
             ;; ie. in  ~/bin/myfile-opener
             ;; #! /bin/sh
             ;; emacsclient -n +$2 $1
             ;;
             ;; :open-file-command "myfile-opener"

             ;; if you want to disable the REPL
             ;; :repl false

             ;; to configure a different figwheel logfile path
             ;; :server-logfile "tmp/logs/figwheel-logfile.log"
             }

  :garden {:builds [{:source-paths ["src/command_line_theme/styles/all"]
                     :stylesheet om-command-line-theme.styles.all/all
                     :compiler {:output-to "resources/public/css/acommand—line-theme.css"
                                :pretty-print? false}}]})
